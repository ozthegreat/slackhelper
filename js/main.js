// Create Module
var slackApp = angular.module('SlackApp', ['ipCookie']);

// Only really going to need one controller
slackApp.controller('slackMainController', ['$scope', '$http', 'ipCookie', function($scope, $http, ipCookie)
{
	// User's Slack API token to use
	$scope.slacktoken = ( ipCookie('slacktoken') != 'undefined' ? ipCookie('slacktoken') : '' );

	// Store any errors here
	$scope.errors = {};

	// Holds any user details found
	$scope.user = {};

	// Holds any files found
	$scope.files = {};

	// Holds summary fo the files
	$scope.filesSummary = {
						'total_files': 0,
						'total_selected': 0,
						'total_size': 0
					};

	// Default order
	$scope.predicate = '-timestamp';

	// Whetehr to remember the Slacktoken or not
	$scope.rememberSlacktoken = false;

	// Whether all the files are selected or not
	$scope.selectedAll = false;

	/**
	 * Tries to get all the User's Slack files
	 * @return {[type]} [description]
	 */
	$scope.getFiles = function()
	{
		// Make sure the error array is empty
		// before each atempt
		$scope.errors = {};

		// If no token is provided
		if ( typeof $scope.slacktoken == 'undefined' || $scope.slacktoken.length == 0 )
		{
			$scope.errors.slacktoken = 'Please enter a valid Slack Token';
			return;
		}

		// If they want to remember the slacktoken
		if ( $scope.rememberSlacktoken )
		{
			ipCookie('slacktoken', $scope.slacktoken, { expires: 365 } );
		}

		// We are working out which User it is by their Token.
		// We can use the auth.tester method to do this
		var authResponsePromise = $http.get('https://slack.com/api/auth.test?token=' + $scope.slacktoken);

		authResponsePromise.success(function(data, status, headers, config) {

			// If there's an error
			if ( data.ok == false )
			{
				$scope.errors.files = data.error;
				return;
			}

			$scope.user = data;
		});

		authResponsePromise.error(function(data, status, headers, config) {
			$scope.errors.files = 'Request failed. Please try again.';
		});

		authResponsePromise.then(function() {

			// Now make the actual request to get the User's files
			var responsePromise = $http.get('https://slack.com/api/files.list?token=' + $scope.slacktoken + '&count=9999&user=' + $scope.user.user_id);

			responsePromise.success(function(data, status, headers, config) {

				// If there's an error
				if ( data.ok == false )
				{
					$scope.errors.files = data.error;
					return;
				}

				$scope.files = data.files;

				// Workout some statistics
				$scope.filesSummary.total_files = data.files.length;

				$scope.filesSummary.total_size = 0;

				$scope.filesSummary.total_selected = 0;

				data.files.forEach(function(file){

					// Add up the size
					$scope.filesSummary.total_size += file.size;

				});

			});

			responsePromise.error(function(data, status, headers, config) {
				$scope.errors.files = 'Request failed. Please try again.';
			});

		});

	}

	/**
	 * Deletes all the selected files
	 * @return {[type]} [description]
	 */
	$scope.deleteFiles = function()
	{
		$scope.files.forEach(function(file) {

			if ( file.checked ) {

				// We are working out which User it is by their Token.
				// We can use the auth.tester method to do this
				var authResponsePromise = $http.get('https://slack.com/api/files.delete?token=' + $scope.slacktoken + '&file=' + file.id);

				authResponsePromise.success(function(data, status, headers, config) {

					// If there's an error
					if ( data.ok == false )
					{
						$scope.errors.files = data.error;
					}

					$scope.user = data;
				});

				authResponsePromise.error(function(data, status, headers, config) {
					$scope.errors.files = 'Request failed. Please try again.';
				});

				// Remove from our files array
				// Updated all the counts
				authResponsePromise.then(function(data, status, headers, config) {

					$scope.filesSummary.total_files--;

					$scope.filesSummary.total_selected--;

					$scope.filesSummary.total_size = $scope.filesSummary.total_size - file.size;

					$scope.files.splice(file.id, 1);

				});

	 		}

		});
	};

	// Simple check all / none functionality
	// Also updates the count
	$scope.checkAll = function () {
		$scope.files.forEach(function(file) {
			file.checked = $scope.selectedAll;
		});

		$scope.filesSummary.total_selected = $scope.selectedAll ? $scope.files.length : 0 ;
	};

	// Updates the total selected count for when an
	// individual checkbox is changed
	$scope.updateCount = function () {
		$scope.filesSummary.total_selected = 0;
		$scope.files.forEach(function(file) {
			if ( file.checked )
				$scope.filesSummary.total_selected++;
		});
	};


}]);

// Filter to convert bytes to human sizes
slackApp.filter('prettySize', function () {
	return function (bytes) {
		if(bytes == 0) return '0 Byte';
		var k = 1000;
		var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
		var i = Math.floor(Math.log(bytes) / Math.log(k));
		return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
	};
});
